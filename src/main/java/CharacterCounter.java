import java.util.*;


public class CharacterCounter {

    public HashMap<Character, Integer> counter(String string) {
        HashMap<Character, Integer> letters = new HashMap<Character, Integer>();
        char[] signs = string.toUpperCase().replaceAll(" ", "").toCharArray();
        for (int i = 0; i < signs.length; i++) {
            if (letters.containsKey(signs[i])) {
                letters.put(signs[i], letters.get(signs[i]) + 1);
            } else letters.put(signs[i], 1);
        }


        return letters;
    }


}
