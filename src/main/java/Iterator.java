import org.w3c.dom.Node;

public class Iterator<E> implements java.util.Iterator<E> {

    private E[] strings;
    private int index;
    private int size;


    Iterator(E[] strings) {
        this.strings = strings.clone();
        index = 0;
        size = strings.length;
    }


    public boolean hasNext() {

        return index < size && strings[index] != null;


    }


    public E next() {
        if (index < size && strings[index] != null) {
            return strings[index++];
        } else
            return null;
    }


    public void remove() {
        strings[index] = null;
    }
    //Реализовать Iterator<String> для обычного массива;
// Продемонстрировать работу получившегося Iterator;
}
