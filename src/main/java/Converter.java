import java.util.Stack;

public class Converter {

    private Stack stack = new Stack();

    public String converting(String string) {
        String result = "";
        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (isNumeric(chars[i])) {
                result += chars[i];
            }
            //if (chars[i]=="/^(?>\pL\pM*)+$/u")result += "Неправильные  данные, могут присутсвовать цифры и знаки +/-*()";
            switch (chars[i]) {

                case ('+'): {
                    if (!stack.isEmpty()) {
                        while (getPriority("+") <= getPriority(String.valueOf(stack.peek()))) result += stack.pop();
                    }
                    stack.push('+');
                    break;
                }
                case ('-'): {
                    if (!stack.isEmpty()) {
                        while (getPriority("-") <= getPriority(String.valueOf(stack.peek()))) result += stack.pop();
                    }
                    stack.push('-');
                    break;

                }
                case ('/'): {
                    if (!stack.isEmpty()) {
                        while (getPriority("/") <= getPriority(String.valueOf(stack.peek()))) result += stack.pop();
                    }
                    stack.push('/');
                    break;

                }
                case ('*'): {

                    if (!stack.isEmpty()) {
                        while (getPriority("*") <= getPriority(String.valueOf(stack.peek()))) result += stack.pop();
                    }
                    stack.push('*');
                    break;

                }
                case ('('): {
                    stack.push(chars[i]);
                    break;
                }
                case (')'): {
                    /*while (!stack.peek().equals("(")) {
                        result+=stack.pop();
                        if (stack.isEmpty()) {
                            System.out.println("Скобки не согласованы.");}}
                    break;

                     */
                    while (!stack.isEmpty()) {
                        if ((char) stack.peek() != '(') {
                            result += stack.pop();
                        } else {
                            stack.pop();
                            if (stack.isEmpty()) System.out.println("Скобки не согласованы.");
                            else result += stack.pop();
                        }

                    }
                }
                break;
            }
        }


        while (!stack.isEmpty()) result += stack.pop();
        return result;
    }

    private static boolean isNumeric(char s) throws NumberFormatException {
        try {
            Integer.parseInt(String.valueOf(s));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static int getPriority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }
}












