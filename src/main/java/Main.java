import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;


public class Main {
    public static void main(String[] args) {

        System.out.println("Задание 1: ");
        String[] names = new String[5];
        names[0] = "Anton";
        names[1] = "Petya";
        names[2] = "Vasiliy";
        names[3] = "Vasilisa";
        names[4] = "Petya";
        Iterator<String> it = new Iterator<String>(names);
        while (it.hasNext()) {
            String name = it.next();
            System.out.println("name = " + name);

        }


        System.out.println("\nЗадание 2: ");
        Converter converter = new Converter();
        System.out.println(converter.converting("3+4"));
        System.out.println(converter.converting("3 + 4 * 2 / (1 - 5)"));


        System.out.println("\nЗадание 3: ");
        new CharacterCounter().counter("Как у тебя дела?").entrySet().stream()
                .sorted(Map.Entry.<Character, Integer>comparingByValue().reversed())
                .forEach(System.out::println); // или любой другой конечный метод


    }
}

